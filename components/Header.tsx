import React, { useState, useEffect, Fragment } from "react";
import Link from "next/link";
import ReactGA from "react-ga";
import { useRouter } from "next/router";
import { AdHeader } from "./ads";

const Header = (props) => {
  if (typeof window !== "undefined") {
    ReactGA.initialize("UA-81632950-3 ");
    ReactGA.pageview(window.location.pathname + window.location.search);
  }
  const router = useRouter();
  // console.log("locals", router.locales);
  return (
    <Fragment>
      <div className="mdk-header-layout js-mdk-header-layout">
        <div id="header" className="mdk-header bg-dark js-mdk-header m-0">
          <div className="mdk-header__content">
            <div
              className="navbar navbar-expand-sm navbar-main navbar-dark bg-primary pl-md-0 pr-0"
              id="navbar"
              data-primary
            >
              <div className="container">
                <button
                  className="navbar-toggler navbar-toggler-custom d-lg-none d-flex mr-navbar"
                  type="button"
                  data-toggle="sidebar"
                >
                  <span className="material-icons">short_text</span>
                </button>

                <div className="d-flex sidebar-account flex-shrink-0 mr-auto mr-lg-0">
                  <a
                    href="/"
                    className="flex d-flex align-items-center text-underline-0"
                  >
                    <img
                      className="img-responsive"
                      src="/logo.png"
                      alt="gcequiz"
                      style={{ height: "50px", width: "50px" }}
                    />
                  </a>
                </div>

                {/* <form
                  className="search-form search-form--light d-none d-sm-flex flex ml-3"
                  action="fixed-index.html"
                >
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search"
                  />
                  <button className="btn" type="submit">
                    <i className="material-icons">search</i>
                  </button>
                </form> */}

                <div className="dropdown">
                  <a
                    href="#account_menu"
                    className="dropdown-toggle navbar-toggler navbar-toggler-dashboard border-left d-flex align-items-center ml-navbar"
                    data-toggle="dropdown"
                  >
                    <img
                      src="/uk.png"
                      className="img-responsive"
                      width="32"
                      alt="gcequiz-uk"
                      style={{ height: "30px", width: "30px" }}
                    />
                    <span className="ml-3 d-flex-inline">
                      <span className="text-light">Eng.</span>
                    </span>
                  </a>
                  <div
                    id="company_menu"
                    className="dropdown-menu dropdown-menu-center navbar-company-menu"
                  >
                    <div className="dropdown-item d-flex align-items-center py-1 navbar-company-info py-1">
                      <span className="mr-3">
                        <img
                          src="/uk.png"
                          className="img-responsive"
                          width="32"
                          alt="gcequiz-uk"
                          style={{ height: "30px", width: "30px" }}
                        />
                      </span>
                      <span className="flex d-flex flex-column">
                        <strong className="h6 m-0">UK</strong>
                      </span>
                    </div>
                    <div className="dropdown-divider"></div>
                    <div className="dropdown-item d-flex align-items-center py-1 navbar-company-info py-1">
                      <span className="mr-3">
                        <img
                          src="/us.png"
                          className="img-responsive"
                          width="32"
                          alt="gcequiz-uk"
                          style={{ height: "30px", width: "30px" }}
                        />
                      </span>
                      <span className="flex d-flex flex-column">
                        <strong className="h6 m-0">US</strong>
                      </span>
                    </div>
                    <div className="dropdown-divider"></div>
                    <div className="dropdown-item d-flex align-items-center py-1 navbar-company-info py-1">
                      <span className="mr-3">
                        <img
                          src="/in.png"
                          className="img-responsive"
                          width="32"
                          alt="gcequiz-uk"
                          style={{ height: "30px", width: "30px" }}
                        />
                      </span>
                      <span className="flex d-flex flex-column">
                        <strong className="h6 m-0">IN</strong>
                      </span>
                    </div>
                  </div>
                </div>

                {/*   <div className="dropdown">
                  <a
                    href="#account_menu"
                    className="dropdown-toggle navbar-toggler navbar-toggler-dashboard border-left d-flex align-items-center ml-navbar"
                    data-toggle="dropdown"
                  >
                    <img
                      src="/assets/images/avatar/demi.png"
                      className="rounded-circle"
                      width="32"
                      alt="Frontted"
                    />
                    <span className="ml-1 d-flex-inline">
                      <span className="text-light">Adrian D.</span>
                    </span>
                  </a>
                  <div
                    id="company_menu"
                    className="dropdown-menu dropdown-menu-center navbar-company-menu"
                  >
                    <div className="dropdown-item d-flex align-items-center py-2 navbar-company-info py-3">
                      <span className="mr-3">
                        <img
                          src="/assets/images/frontted-logo-blue.svg"
                          width="43"
                          height="43"
                          alt="avatar"
                        />
                      </span>
                      <span className="flex d-flex flex-column">
                        <strong className="h5 m-0">Adrian D.</strong>
                        <small className="text-muted text-uppercase">
                          STUDENT
                        </small>
                      </span>
                    </div>
                    <div className="dropdown-divider"></div>
                    <a
                      className="dropdown-item d-flex align-items-center py-2"
                      href="fixed-student-edit-account.html"
                    >
                      <span className="material-icons mr-2">
                        account_circle
                      </span>{" "}
                      Edit Account
                    </a>
                    <a
                      className="dropdown-item d-flex align-items-center py-2"
                      href="#"
                    >
                      <span className="material-icons mr-2">settings</span>{" "}
                      Settings
                    </a>
                    <a
                      className="dropdown-item d-flex align-items-center py-2"
                      href="login.html"
                    >
                      <span className="material-icons mr-2">exit_to_app</span>{" "}
                      Logout
                    </a>
                  </div>
  </div> **/}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="page__header page__header-nav mb-0">
        <div className="container page__container">
          <div
            className="navbar navbar-secondary navbar-light navbar-expand-sm p-0 d-none d-md-flex"
            id="secondaryNavbar"
          >
            <ul className="nav navbar-nav ">
              <li className="nav-item ">
                <a href="/" className="nav-link ">
                  Home
                </a>
              </li>
              <li className="nav-item ">
                <a
                  href="/category/ks1-key-stage-1-online-quizzes"
                  className="nav-link "
                >
                  KS1
                </a>
              </li>
              <li className="nav-item ">
                <a
                  href="/category/ks2-key-stage-2-online-quizzes"
                  className="nav-link "
                >
                  KS2
                </a>
              </li>
              <li className="nav-item ">
                <a href="/category/ks3-quizzes-online" className="nav-link ">
                  KS3
                </a>
              </li>

              <li className="nav-item ">
                <a href="/category/gcse-quizzes-online" className="nav-link ">
                  GCSE
                </a>
              </li>
              <li className="nav-item ">
                <a href="/category/a-level-quiz" className="nav-link ">
                  A Level
                </a>
              </li>

              <li className="nav-item ">
                <a
                  href="/category/online-o-level-quizzes"
                  className="nav-link "
                >
                  O Level
                </a>
              </li>
              <li className="nav-item ">
                <a href="/blog" className="nav-link ">
                  Blog
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {router.asPath !== "/" && (
        <section className="uk-section-xsmall">
          <div className="container">
            <div className="d-flex justify-content-center  mt-5 pt-2 ">
              <AdHeader />
            </div>
          </div>
        </section>
      )}
    </Fragment>
  );
};

export default Header;
