import Head from "next/head";

import Link from "next/link";

const Footer = (props) => {
  return (
    <div>
      <div className="bg-dark text-white" id="footer">
        <div className="container page__container">
          <div className="row">
            <div className="col-md-3">
              <a href="" className="brand d-flex align-items-center mb-4">
                <img
                  className="img-responsive"
                  src="/logo-color.png"
                  alt="gcequiz"
                  style={{ height: "60px", width: "60px" }}
                />
              </a>
            </div>
            <div className="col-md-9">
              <div className="row">
                <div className="col-sm-6 col-md-3">
                  <h5>Student</h5>
                  {/* <ul className="list-group list-group-flush mb-3">
                    <li className="">
                      <a href="#">Quizzes</a>
                    </li>
                    <li className="">
                      <a href="#">Take Quizzes</a>
                    </li>
                    <li className="">
                      <a href="#">Profile</a>
                    </li>
                    <li className="">
                      <a href="#">Edit Account</a>
                    </li>
                  </ul> */}
                </div>
                <div className="col-sm-6 col-md-3">
                  <h5>Instructor</h5>
                  {/* <ul className="list-group list-group-flush mb-3">
                    <li className="">
                      <a href="#">Dashboard</a>
                    </li>
                    <li className="">
                      <a href="#">Edit Quizzes</a>
                    </li>
                    <li className="">
                      <a href="#">Profile</a>
                    </li>
                    <li className="">
                      <a href="#">Add Quizzes</a>
                    </li>
                  </ul> */}
                </div>
                <div className="col-sm-6 col-md-3">
                  <h5>Account</h5>
                  <ul className="list-group list-group-flush mb-3">
                    <li className="">
                      <a href="/signin">Login</a>
                    </li>
                    <li className="">
                      <a href="/signup">Sign up</a>
                    </li>
                  </ul>
                </div>
                <div className="col-sm-6 col-md-3">
                  <h5>Contact us</h5>
                  <p className="text-light">
                    Efoulan, Yaounde
                    <br />
                    Cameroon, Email:info@gcequiz.com
                  </p>

                  <div className="d-flex">
                    <a
                      href="https://www.facebook.com/gcerevision"
                      className="btn btn-facebook btn-rounded-social d-flex align-items-center justify-content-center mr-2"
                    >
                      <i className="fab fa-facebook"></i>
                    </a>
                    <a
                      href="https://twitter.com/cgcerevision"
                      className="btn btn-twitter btn-rounded-social d-flex align-items-center justify-content-center mr-2"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>
                    {/* <a
                      href=""
                      className="btn btn-secondary btn-rounded-social d-flex align-items-center justify-content-center mr-2"
                    >
                      <i className="fab fa-github"></i>
                    </a> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
