import React, { useState, useEffect, Fragment } from "react";

import Head from "next/head";
import Footer from "./Footer";
import Header from "./Header";

const Layout = (props) => {
  return (
    <React.Fragment>
      <body className="layout-fixed layout-sticky-subnav">
        <Header />
        {props.children}
        <Footer />
      </body>
    </React.Fragment>
  );
};

export default Layout;
