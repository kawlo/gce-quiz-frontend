import React, { useEffect } from "react";

const AdHeader = () => {
  useEffect(() => {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <ins
      className="adsbygoogle"
      style={{
        display: "inline-block",
        width: "820px",
        height: "100px",
      }}
      data-ad-client="ca-pub-6915392312901512"
      data-ad-slot="3194597380"
    ></ins>
  );
};

const AdBanner = () => {
  useEffect(() => {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <ins
      className="adsbygoogle"
      style={{ display: "block" }}
      data-ad-client="ca-pub-6915392312901512"
      data-ad-slot="9425738077"
      data-ad-format="auto"
      data-full-width-responsive="true"
    ></ins>
  );
};

export { AdHeader, AdBanner };
