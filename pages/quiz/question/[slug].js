import React, { Fragment, useState } from "react";
import { useRouter } from "next/router";
import { initializeApollo } from "../../../lib/useApollo";
import ReactHtmlParser from "react-html-parser";
import { NextSeo } from "next-seo";
import { QUIZ_BY_SLUG_QUERY } from "../../../graphql/queries";
import { AdBanner } from "../../../components/ads";

const RenderQuestion = (item, index) => {
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [selected, setSelected] = useState(null);
  //(console.log("selected", item);

  const submitAnswer = () => {
    const ans = item.answers[selected - 1];
    if (ans.isCorrect) {
      setSuccess(true);
    } else {
      setError(true);
    }
  };

  return (
    <Fragment>
      <div className="card mb-4" data-position="1" data-question-id="1">
        <div className="card-header d-flex justify-content-between">
          <div className="d-flex align-items-center ">
            <span className="question_handle btn btn-sm btn-secondary">
              {" "}
              {index + 1}:
            </span>{" "}
            <div className="h5 m-0 ml-4">{ReactHtmlParser(item.content)}</div>
          </div>
        </div>
        <br />
        <div className="card-header d-flex  justify-content-center">
          {!!item.image ? (
            <img
              src={item.image.url}
              alt={item.content}
              className="img-fluid"
            />
          ) : null}
        </div>
        <div className="card-body">
          <div id="answerWrapper_1" className="mb-4">
            <div className="row mb-1">
              <div className="col">
                <strong></strong>
              </div>
              <div className="col text-right">
                <strong>Correct</strong>
              </div>
            </div>

            <form action="#">
              <ul className="list-group" id="answer_container_1">
                {item.answers.map((ans, i) => {
                  return (
                    <Fragment>
                      <li className="list-group-item d-flex">
                        <span className="question_handle btn btn-sm btn-secondary mr-1">
                          {i + 1}:
                        </span>
                        <div>{ReactHtmlParser(ans.text)}</div>

                        <div className="ml-auto">
                          <input
                            type="radio"
                            name="question[correct_answer_id]"
                            id="correct_answer_1"
                            onChange={(e) => {
                              setSelected(e.currentTarget.value);
                            }}
                            value={i + 1}
                          />
                        </div>
                      </li>

                      {!!ans.image ? (
                        <div className="card-header d-flex  justify-content-center">
                          <img
                            src={ans.image.url}
                            alt={item.text}
                            className="img-fluid"
                          />
                        </div>
                      ) : null}
                    </Fragment>
                  );
                })}
              </ul>
            </form>
          </div>

          <div className="">
            <a
              onClick={() => {
                if (selected) {
                  submitAnswer();
                } else {
                  alert("Please selected an answer");
                }
              }}
              className="btn btn-success text-white"
            >
              Submit Answer
            </a>
          </div>
          {success && (
            <div
              class="alert alert-dismissible bg-success text-white border-0 fade show mt-2"
              role="alert"
            >
              <button
                type="button"
                className="close"
                data-dismiss="alert"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Bravo - </strong> {selected} is the correct Answer!
            </div>
          )}
          {error && (
            <div
              className="alert alert-dismissible bg-danger text-white border-0 fade show mt-2"
              role="alert"
            >
              <button
                type="button"
                className="close"
                data-dismiss="alert"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Wrong - </strong> {selected} is not the Answer!
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};
const Question = ({ quiz }) => {
  const router = useRouter();

  const { slug } = router.query;

  return (
    <Fragment>
      <NextSeo
        title={quiz.title}
        description={quiz.meta}
        openGraph={{
          type: quiz.title,
          description: quiz.meta,
          url: "https://gcequiz.com" + router.asPath,
          images: [
            {
              url: "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png",
              alt: quiz.title,
            },
          ],
        }}
      />
      <div className="container page__heading-container mt-5 pt-2">
        <div className="page__heading d-flex align-items-center justify-content-between">
          <h1 className="m-0">{quiz.title}</h1>
          <a href="" className="btn btn-success ml-3">
            {quiz.questions.length} Questions
          </a>
        </div>
      </div>

      <div className="container page__container">
        <div className="row">
          <div className="col-md-8">
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-header card-header-large bg-light d-flex align-items-center">
                    <div className="flex">
                      <h4 className="card-header__title">Quiz Description</h4>
                    </div>
                  </div>
                  <div className="card-body">
                    <p>{ReactHtmlParser(quiz.description)}</p>
                  </div>
                </div>
              </div>

              <div className="col-md-12">
                {quiz.questions.map((item, index) =>
                  RenderQuestion(item, index)
                )}
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="card ">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Feature Image</h4>
                </div>
              </div>
              <div className="card-body">
                <div className="card card__course clear-shadow border">
                  <div className=" d-flex justify-content-center">
                    <a className="" href="#">
                      <img
                        src="https://images.unsplash.com/photo-1562577309-4932fdd64cd1?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=clamp&amp;w=800&amp;h=250"
                        style={{ width: "100%" }}
                        alt={quiz.title}
                      />
                    </a>
                  </div>
                </div>
              </div>

              <div>
                <AdBanner />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export const getServerSideProps = async (context) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: QUIZ_BY_SLUG_QUERY,
    variables: { filter: { slug: context.query.slug } },
  });
  const quiz = data.quizOne;
  //console.log(quiz, "quiz");
  return { props: { quiz } };
};

export default Question;
