import React, { useState, Fragment } from "react";
import { useRouter } from "next/router";
import { initializeApollo } from "../../lib/useApollo";
import ReactHtmlParser from "react-html-parser";
import { AdBanner } from "../../components/ads";

import { CATEGORY_BY_SLUG_QUERY, QUIZ_MANY_QUERY } from "../../graphql/queries";

import { NextSeo } from "next-seo";

const Quiz = ({ category, quiz }) => {
  const [id, setId] = useState();
  const router = useRouter();
  const truncate = (input) =>
    input.length > 100 ? `${input.substring(0, 100)}...` : input;
  const renderCategory = (data) => {
    return (
      <Fragment>
        <div className="row">
          {!!data &&
            data.quizMany.map((item) => (
              <div className="col-md-4" key={item._id}>
                <div className="card card__course">
                  <div className="card-header card-header-large card-header-dark bg-dark d-flex text-decoration-none justify-content-center">
                    <a
                      className="card-header__title justify-content-center align-self-center d-flex flex-column"
                      href={"https://gcequiz.com/quiz/question/" + item.slug}
                    >
                      <span>
                        <img
                          src={
                            item.image
                              ? item.image.url
                              : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                          }
                          className="mb-1 img-fluid"
                          alt={item.title}
                        />
                      </span>
                    </a>
                  </div>
                  <div className="p-3">
                    <h5 className="font-weight-bold">{item.title}</h5>

                    <a
                      href={"https://gcequiz.com/quiz/question/" + item.slug}
                      className="text-body text-decoration-none"
                    >
                      <div className="d-flex align-items-center">
                        <strong className="h6 m-0">
                          {truncate(item.meta)}
                        </strong>
                      </div>
                      <div className="d-flex align-items-center">
                        <a className="btn btn-primary text-white mt-1">
                          Try quiz
                        </a>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </Fragment>
    );
  };

  return (
    <React.Fragment>
      <NextSeo
        title={category.name}
        description={category.meta}
        openGraph={{
          type: category.name,
          description: category.meta,
          url: "https://gcequiz.com" + router.asPath,
          images: [
            {
              url: category.image
                ? category.image.url
                : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png",
              alt: category.name,
            },
          ],
        }}
      />

      <div className="container page__heading-container mt-5 pt-2">
        <div className="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
          <div>
            <h1 className="m-lg-0">{category.name}</h1>
            <div className="d-inline-flex align-items-center"></div>
          </div>
          <div></div>
        </div>
      </div>

      <div className="container page__container">
        <div className="row">
          <div className="col-md-8">
            {renderCategory(quiz)}
            <div className="card">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Quiz Description</h4>
                </div>
              </div>
              <div className="card-body">
                <p>{ReactHtmlParser(category.content)}</p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="card ">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Feature Image</h4>
                </div>
              </div>
              <div className="card-body">
                <div className="card card__course clear-shadow border">
                  <div className=" d-flex justify-content-center">
                    <a className="" href="#">
                      <img
                        src={
                          category.image
                            ? category.image.url
                            : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                        }
                        style={{ width: "100%" }}
                        alt={category.name}
                      />
                    </a>
                  </div>
                </div>
              </div>

              <div>
                <AdBanner />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container page__container">
        <hr />
      </div>
    </React.Fragment>
  );
};
export const getServerSideProps = async (context) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: CATEGORY_BY_SLUG_QUERY,
    variables: { filter: { slug: context.query.slug } },
  });
  const category = data.categoryOne;

  const { data: quiz } = await apolloClient.query({
    query: QUIZ_MANY_QUERY,
    variables: {
      filter: {
        OR: [
          { categoryId: category._id },
          { _operators: { categoryId: { in: category.childrenIds } } },
        ],
      },
    },
  });
  return { props: { category, quiz } };
};
export default Quiz;
