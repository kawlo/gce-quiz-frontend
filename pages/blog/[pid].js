import React, { useState } from "react";
import { initializeApollo } from "../../lib/useApollo";
import ReactHtmlParser from "react-html-parser";
import { NextSeo } from "next-seo";
import { ARTICLE_QUERY_BY_ONE } from "../../graphql/queries";

import { useRouter } from "next/router";
import moment from "moment";

const Blogrender = ({ article }) => {
  const router = useRouter();
  const { pid } = router.query;

  return (
    <React.Fragment>
      <NextSeo
        title={article.title}
        description={article.meta}
        openGraph={{
          type: article.title,
          description: article.meta,
          url: "https://gcequiz.com" + router.asPath,
          images: [
            {
              url: article.featuredImage
                ? article.featuredImage.url
                : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png",
              alt: article.title,
            },
          ],
        }}
      />
      <div className="container page__heading-container mt-5 pt-2">
        <div className="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
          <div>
            <h1 className="m-lg-0">{article.title}</h1>
            <div className="d-inline-flex align-items-center"></div>
          </div>
          <div></div>
        </div>
      </div>
      <div className="container page__container">
        <div className="row">
          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">
                    Posted on the {moment(article.createdAt).format("LLL")}
                  </h4>
                </div>
              </div>
              <div className="card-body">
                <p>{ReactHtmlParser(article.content)}</p>
              </div>

              <div class="page-separator">
                <div class="page-separator__text">Written by </div>
              </div>
              <div className="mx-auto">
                <div class="avatar avatar-x avatar-online">
                  <img
                    src={article.createdBy.avatar.url}
                    alt={article.createdBy.name}
                    class="avatar-img rounded-circle"
                  />
                </div>
                <a>
                  <span className="ml-1 d-flex-inline">
                    <span className="">{article.createdBy.name}</span>
                  </span>
                </a>
                <br />
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="card ">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Feature Image</h4>
                </div>
              </div>
              <div className="card-body">
                <div className="card card__course clear-shadow border">
                  <div className=" d-flex justify-content-center">
                    <a className="" href="#">
                      <img
                        src={
                          article.featuredImage
                            ? article.featuredImage.url
                            : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                        }
                        style={{ width: "100%" }}
                        alt={article.title}
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export const getServerSideProps = async (context) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: ARTICLE_QUERY_BY_ONE,
    variables: { filter: { slug: context.query.pid } },
  });
  const article = data.articleOne;
  // console.log(articles, "articles");
  return { props: { article } };
};

export default Blogrender;
