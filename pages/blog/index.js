import React, { useState } from "react";
import { initializeApollo } from "../../lib/useApollo";
import ReactHtmlParser from "react-html-parser";

import { ARTICLE_QUERY_PAGINATION } from "../../graphql/queries";

import Meta from "../../components/seo-meta";
import { useRouter } from "next/router";

export default ({ articles }) => {
  const router = useRouter();

  // console.log(data);
  const truncate = (input) =>
    input.length > 500 ? `${input.substring(0, 500)}...` : input;
  return (
    <React.Fragment>
      <Meta
        title={"Gcequiz blog | Latest tips and tricks on anwsering mcqs"}
        desc={
          "All tips and tricks on how to answer gcse quizzes, exams mcqs and many more."
        }
        canonical={router.asPath}
      />
      <div className="container page__heading-container mt-5 pt-5">
        <div className="page__heading d-flex align-items-center justify-content-between">
          <h1 className="m-0">Latest Blog Posts</h1>
        </div>
      </div>

      <div className="container page__container">
        <div className="row">
          <div className="col-md-8">
            <div className="row">
              {articles.map((item, index) => (
                <div className="col-md-12">
                  <div className="card">
                    <div className="px-4 py-3">
                      <a
                        title={item.title}
                        className="h4 my-2 text-body text-decoration-none"
                        href={"/blog/" + item.slug}
                      >
                        {item.title}
                      </a>
                      <div className="flex mb-1">
                        <img
                          src={
                            !!item.featuredImage
                              ? item.featuredImage.url
                              : "/img/gcequiz.com.jpg"
                          }
                          alt={item.title}
                          className="rounded card-img-top"
                        />
                        <div className="flex">
                          <a
                            href={"/blog/" + item.slug}
                            className=" text-body decoration-0"
                          >
                            <p className="">
                              {ReactHtmlParser(truncate(item.content))}
                            </p>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>

          <div className="col-md-4">
            <div className="card ">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Feature Image</h4>
                </div>
              </div>
              <div className="card-body">
                <div className="card card__course clear-shadow border">
                  <div className=" d-flex justify-content-center">
                    <a className="" href="#">
                      <img
                        src="https://images.unsplash.com/photo-1562577309-4932fdd64cd1?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=clamp&amp;w=800&amp;h=250"
                        style={{ width: "100%" }}
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export const getServerSideProps = async (context) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: ARTICLE_QUERY_PAGINATION,
    variables: { page: 1 },
  });
  const articles = data.articlePagination.items;

  return { props: { articles } };
};
