import React from "react";
//fetch = require("isomorphic-fetch");
// import { CATEGORY_QUERY } from "../graphql/category.mutation";
// import { useQuery } from "@apollo/react-hooks";

import fetch from "isomorphic-fetch";
import {
  CATEGORY_BY_ID_QUERY_SITEMAP,
  CATEGORY_BY_SLUG_QUERY,
  QUIZ_MANY_QUERY,
  QUIZ_MANY_QUERY_SITEMAP,
} from "../graphql/queries";
import { initializeApollo } from "../lib/useApollo";
const EXTERNAL_DATA_URL = "https://gcequiz.com";

//import { NextPageContext } from "next";

const blogPostsXml = (blogPosts) => {
  let latestPost = 0;
  let postsXml = "";
  blogPosts.articleMany.map((post) => {
    const postDate = !!post.updatedAt ? post.updatedAt : post.createdAt;
    if (!latestPost || postDate > latestPost) {
      latestPost = postDate;
    }
    postsXml += `
    <url>
      <loc>https://gcequiz.com/blog/${post.slug}</loc>
      <lastmod>${postDate}</lastmod>
      <priority>0.50</priority>
    </url>`;
  });
  return {
    postsXml,
  };
};

// const quizXml = (blogPosts) => {
//   let latestPost = 0;
//   let quizsXml = "";
//   blogPosts.quizMany.map((post) => {
//     const postDate = !!post.updatedAt ? post.updatedAt : post.createdAt;

//     if (!latestPost || postDate > latestPost) {
//       latestPost = postDate;
//     }
//     quizsXml += `
//     <url>
//       <loc>https://gcequiz.com/quiz/question/${post.slug}</loc>
//       <lastmod>${postDate}</lastmod>
//       <priority>0.50</priority>
//     </url>`;
//   });
//   return {
//     quizsXml,
//     latestPost,
//   };
// };
// const categoriesXml = (blogPosts) => {
//   let latestPost = 0;
//   let categoryXml = "";
//   blogPosts.categoryMany.map((post) => {
//     const postDate = !!post.updatedAt ? post.updatedAt : post.createdAt;

//     if (!latestPost || postDate > latestPost) {
//       latestPost = postDate;
//     }

//     categoryXml += `
//       <url>
//         <loc>https://gcequiz.com/quiz/${post.slug}</loc>
//         <lastmod>${postDate}</lastmod>
//         <priority>0.50</priority>
//       </url>`;
//   });
//   return {
//     categoryXml,
//   };
// };

const sitemapXml = (site, blogPosts) => {
  const { postsXml } = blogPostsXml(blogPosts);
  // const { quizsXml, latestPost } = quizXml(quiz);
  // const { categoryXml } = categoriesXml(categories);

  return `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
      <loc>https://gcequiz.com/</loc>
    
      <priority>1.00</priority>
    </url>
      <url>
      <loc>https://gcequiz.com/signin</loc>
      <priority>0.8</priority>
    </url>
    <url>
      <loc>https://gcequiz.com/signup</loc>
      <priority>0.8</priority>
    </url>
    <url>
    <loc>https://gcequiz.com/blog</loc>
    <priority>0.8</priority>
  </url>

  ${site.map((item) => item)}

  ${postsXml}
  </urlset>`;
};

const Sitemap = () => {};

export const getServerSideProps = async ({ context, res }) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: CATEGORY_BY_ID_QUERY_SITEMAP,
    variables: { id: "5fea8d7f3919340e152000cf" },
  });

  const renderSitemap = async (item) => {
    const sitemap = [];

    let big = ` <url>
    <loc>https://gcequiz.com/category/${item.slug}</loc>
  <lastmod>${item.updatedAt}</lastmod>
  <priority>0.50</priority>
</url>`;
    // adding big four (ks1, ks2 etc)
    sitemap.push(big);

    // getting childrend of big four (ks1 --> ks1 history, ks1 maths etc)
    for (const it of item.children) {
      sitemap.push(`<url>
         <loc>https://gcequiz.com/quiz/${it.slug}</loc>
             <lastmod>${it.updatedAt}</lastmod>
             <priority>0.50</priority>
         </url>`);

      // getting quiz from the (ks1)
      const { data: quiz } = await apolloClient.query({
        query: QUIZ_MANY_QUERY_SITEMAP,
        variables: {
          filter: {
            OR: [
              { categoryId: it._id },
              { _operators: { categoryId: { in: it.childrenIds } } },
            ],
          },
        },
      });

      quiz.quizMany.forEach((quest) => {
        sitemap.push(`<url>
     <loc>https://gcequiz.com/quiz/question/${quest.slug}</loc>
     <lastmod>${quest.updatedAt}</lastmod>
     <priority>0.50</priority>
 </url>`);
      });
    }

    return sitemap;
  };

  const sitemap = await Promise.all(
    data.categoryById.children.map(renderSitemap)
  );

  const blog = await fetch("https://v1.api.gcequiz.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: "{ articleMany(limit: 1000){ createdAt,slug,title,updatedAt} }",
    }),
  })
    .then((res) => res.json())
    .then((res) => res.data);

  res.setHeader("Content-Type", "text/xml");
  res.write(sitemapXml(sitemap.flat(), blog));
  res.end();

  return { props: { category: null } };
};

// Sitemap.getInitialProps = async ({ res }) => {
//   const catgories = await fetch("https://v1.api.gcequiz.com/graphql", {
//     method: "POST",
//     headers: { "Content-Type": "application/json" },
//     body: JSON.stringify({
//       query:
//         "{ categoryMany(limit: 1000){ createdAt,slug,childrenIds,updatedAt } }",
//     }),
//   })
//     .then((res) => res.json())
//     .then((res) => res.data);

//   const questions = await fetch("https://v1.api.gcequiz.com/graphql", {
//     method: "POST",
//     headers: { "Content-Type": "application/json" },
//     body: JSON.stringify({
//       query: "{ quizMany(limit: 1000){ createdAt,slug,title,updatedAt } }",
//       variables:` {
//         filter: {
//           OR: [
//             { categoryId: 5fea8d7f3919340e152000cf },
//             { _operators: { categoryId: { in: category.childrenIds } } },
//           ]
//         }
//       }`
//     }),
//   })
//     .then((res) => res.json())
//     .then((res) => res.data);

//   res.setHeader("Content-Type", "text/xml");
//   res.write(sitemapXml(blog, questions, catgories));
//   res.end();
// };

export default Sitemap;
