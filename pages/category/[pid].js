import React, { Fragment } from "react";
import { useRouter } from "next/router";
import { initializeApollo } from "../../lib/useApollo";
import ReactHtmlParser from "react-html-parser";
import { CATEGORY_BY_SLUG_QUERY } from "../../graphql/queries";
import { NextSeo } from "next-seo";
import { AdBanner } from "../../components/ads";

const URLS = [
  "5ee79e1f793f2e1731ed31e3",
  "5f0a105b651ce15f7aef6d3f",
  "5f1aa4881e56bf755595df29",
  "6032dfc8a607ae71f3b8abae",
  "604f4b3c914d6a0b3245c171",
  "60f6b90499df6304dc73db4d",
];

const Category = ({ category }) => {
  const router = useRouter();
  const { pid } = router.query;

  const truncate = (input) =>
    input.length > 100 ? `${input.substring(0, 100)}...` : input;

  const renderCategory = (data) => {
    return (
      <Fragment>
        <div className="row">
          {!!data &&
            data.map((item) => (
              <div className="col-md-3" key={item._id}>
                <div className="card card__course">
                  <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                    <a
                      className="card-header__title justify-content-center align-self-center d-flex flex-column"
                      href={"/quiz/" + item.slug}
                    >
                      <span>
                        <img
                          src={
                            item.image
                              ? item.image.url
                              : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                          }
                          className="mb-1 img-fluid"
                          alt={item.name}
                        />
                      </span>
                    </a>
                  </div>
                  <div className="p-3">
                    <h5 className="font-weight-bold">{item.name}</h5>

                    <a
                      href={"/quiz/" + item.slug}
                      className="text-body text-decoration-none"
                    >
                      <div className="d-flex align-items-center">
                        <strong className="h6 m-0">
                          {truncate(item.meta)}
                        </strong>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </Fragment>
    );
  };
  return (
    <Fragment>
      <NextSeo
        title={category.name}
        description={category.meta}
        openGraph={{
          type: category.name,
          description: category.meta,
          url: "" + router.asPath,
          images: [
            {
              url: category.image
                ? category.image.url
                : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png",
              alt: category.name,
            },
          ],
        }}
      />

      <div className="container page__heading-container mt-5 pt-2">
        <div className="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
          <div>
            <h1 className="m-lg-0">{category.name}</h1>
            <div className="d-inline-flex align-items-center"></div>
          </div>
          <div></div>
        </div>
      </div>

      <div className="container page__container">
        {renderCategory(category.children)}
        <hr />
      </div>
      <div className="container page__container">
        <div className="row">
          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Quiz Description</h4>
                </div>
              </div>
              <div className="card-body">
                <p>{ReactHtmlParser(category.content)}</p>
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div className="card ">
              <div className="card-header card-header-large bg-light d-flex align-items-center">
                <div className="flex">
                  <h4 className="card-header__title">Feature Image</h4>
                </div>
              </div>
              <div className="card-body">
                <div className="card card__course clear-shadow border">
                  <div className=" d-flex justify-content-center">
                    <a className="" href="#">
                      <img
                        src={
                          category.image
                            ? category.image.url
                            : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                        }
                        style={{ width: "100%" }}
                        alt={category.name}
                      />
                    </a>
                  </div>
                </div>
              </div>

              <div>
                <AdBanner />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export const getServerSideProps = async (context) => {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: CATEGORY_BY_SLUG_QUERY,
    variables: { filter: { slug: context.query.pid } },
  });
  const category = data.categoryOne;

  const { res } = context;
  if (!URLS.includes(category._id)) {
    res.writeHead(301, {
      location: `https://gcequiz.com/quiz/${category.slug}`,
    });
    res.end();
  }

  return { props: { category } };
};
export default Category;
