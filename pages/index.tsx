import React, { Fragment } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import dynamic from "next/dynamic";
import { CATEGORY_BY_ID_QUERY } from "../graphql/queries";
import Link from "next/link";
import { NextSeo } from "next-seo";
const Skeleton = dynamic(() => import("react-skeleton-loader"), { ssr: false });

export default function Home() {
  const { data: dataCategory, loading: loadingCategory } = useQuery(
    CATEGORY_BY_ID_QUERY,
    {
      variables: {
        id: "5fea8d7f3919340e152000cf",
      },
    }
  );
  const router = useRouter();

  // console.log("datacCategory", dataCategory);
  const truncate = (input) =>
    input.length > 100 ? `${input.substring(0, 100)}...` : input;
  const renderCategory = (data: any) => {
    return (
      <Fragment>
        <div className="row">
          <div className="col-md-6">
            <div className="text-left m-4 pb-4">
              <a href="#" className="bold h4 text-dark">
                {data.name}
              </a>
            </div>
          </div>
          <div className="col-md-6">
            <div className="text-right m-4 pb-4">
              <Link key={data._id} href={"/category/" + data.slug}>
                <a href="#" className="btn btn-light btn-lg">
                  View more
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="row">
          {!!data &&
            data.children.map((item) => (
              <div className="col-md-3">
                <div className="card card__course">
                  <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                    <a
                      className="card-header__title justify-content-center align-self-center d-flex flex-column"
                      href={"/quiz/" + item.slug}
                    >
                      <span>
                        <img
                          src={
                            item.image
                              ? item.image.url
                              : "https://v1.api.gcequiz.com/media/image/5eeb77ef2fc6173e335a92e4.png"
                          }
                          className="mb-1 img-fluid"
                          alt={item.name}
                        />
                      </span>
                    </a>
                  </div>
                  <div className="p-3">
                    <h5 className="font-weight-bold">{item.name}</h5>
                    <a
                      href={"/quiz/" + item.slug}
                      className="text-body text-decoration-none"
                    >
                      <div className="d-flex align-items-center">
                        <strong className="h6 m-0">
                          {truncate(item.meta)}
                        </strong>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </Fragment>
    );
  };
  return (
    <Fragment>
      <NextSeo
        title={"Gce Quiz - Test your knowledge online"}
        description="gce quiz gives you the quizzes you need to prepare for your exams, with categories such as gcse quizzes,A level, necta quiz, ecz quiz and many more. "
      />

      <div className="mdk-header-layout__content page">
        <div className="home-banner text-white mb-4">
          <div className="container page__container">
            <h1 className="display-4 bold">Everything you need to Study</h1>
            <p className="lead mb-5">With real exams quizzes and questions.</p>
            <div>
              {/* <a
                className="btn btn-light btn-lg mr-1"
                href="fixed-student-courses.html"
              >
                Browse Quizzes
              </a> */}
              {/* <a
                className="btn btn-success btn-lg"
                href="fixed-student-courses.html"
              >
                Create an account
              </a> */}
            </div>
          </div>
        </div>

        <div className="container page__container">
          <h2 className="bold m-4 text-center p-4">Recent Quizzes</h2>

          {!!dataCategory ? (
            dataCategory?.categoryById?.children.map((item) =>
              renderCategory(item)
            )
          ) : (
            <Fragment>
              <div className="row">
                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="card card__course">
                    <div className="card-header card-header-large card-header-dark bg-dark d-flex justify-content-center">
                      <Skeleton count={2} width="100%" />
                    </div>
                    <div className="p-3">
                      <div className="mb-2">
                        <Skeleton count={2} width="100%" />

                        <br />
                        <small className="text-muted">Loading....</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          )}

          <div className="m-4 p-4">
            <h2 className="bold mb-1 text-center">Features &amp; Highlights</h2>
            <p className="lead text-muted text-center">
              What makes GCEQUIZ an awesome
            </p>
          </div>

          <div className="bg-soft-primary card-body mb-4">
            <div className="row p-4">
              <div className="col-md-6 offset-md-3">
                <div className="mb-4">
                  <h4 className="text-center text-primary bold mb-1">
                    Sign up and get new quiz releases
                  </h4>
                  <p className="text-center text-muted">
                    No spam. Only releases, updates and informations
                  </p>
                </div>
                <div className="d-flex">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Your email address"
                  />
                  <a href="#" className="btn btn-secondary ml-2">
                    SUBSCRIBE
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
