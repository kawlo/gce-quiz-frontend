import React from "react";

import { useRouter } from "next/router";
import Meta from "../../components/seo-meta";
import dayjs from "dayjs";
import SecureLS from "secure-ls";
import axios from "axios";

const SignIn = () => {
  const router = useRouter();

  return <SignInView router={router} router={router} />;
};
export default SignIn;

class SignInView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        email: "",
        password: "",
        name: "",
        repassword: "",
      },
      errors: {},
      loading: false,
      done: false,
    };
  }
  storeToken = async (data) => {
    fetch("https://v1.api.gcequiz.com/auth/refresh-access-token", {
      method: "get",
      headers: new Headers({
        "X-Auth": data.addUser.refreshToken,
      }),
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.log(responseData);
      })
      .catch((e) => console.log(e));
  };

  validateForm = () => {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "*Please enter your email-ID.";
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "*Please enter your password.";
    }

    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
        errors["email"] = "*Please enter valid email,e.g watt23@gmail.com";
      }
    }

    this.setState({
      errors: errors,
    });
    this.setState({ loading: formIsValid });
    return formIsValid;
  };

  onsubmit = () => {
    if (this.validateForm()) {
      this.setState({ loading: true });

      axios
        .post("https://v1.api.gcequiz.com/", {
          username: this.state.fields.email,
          password: this.state.fields.password,
        })
        .then(() => {
          this.setState({ loading: false });
        });
    } else {
    }
  };
  handleChange = (e) => {
    e.preventDefault();

    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields,
    });
  };
  render() {
    console.log("data");
    return (
      <React.Fragment>
        <Meta
          title={"Login to gcequiz"}
          desc={
            "Login in our website to be able to save all your quizzes progress, and have a chance to win a priz during our monthly quiz."
          }
          canonical={this.props.router.asPath}
        />
        <div className="container ">
          <div className="row  justify-content-center">
            <div className="col-md-4">
              <div class="mt-5 mb-5">
                <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-4 navbar-light">
                  <a
                    href="index.html"
                    class="text-center text-light-gray mb-4"
                  ></a>
                </div>

                <div class="card card-body">
                  <div class="alert alert-soft-success d-flex" role="alert">
                    <i class="material-icons mr-3">check_circle</i>
                    <div class="text-body">
                      An email with password reset instructions has been sent to
                      your email address, if it exists on our system.
                    </div>
                  </div>

                  <form>
                    <div class="form-group">
                      <label class="text-label" for="email_2">
                        Email Address:
                      </label>
                      <div class="input-group input-group-merge">
                        <input
                          id="email_2"
                          onChange={this.handleChange}
                          name="email"
                          type="email"
                          value={this.state.fields.email}
                          class="form-control form-control-prepended"
                          placeholder="john@doe.com"
                        />
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <span class="far fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                      {!!this.state.errors.email && (
                        <div class="alert alert-danger" role="alert">
                          {this.state.errors.email}
                        </div>
                      )}
                    </div>
                    <div class="form-group">
                      <label class="text-label" for="password_2">
                        Password:
                      </label>
                      <div class="input-group input-group-merge">
                        <input
                          id="password_2"
                          type="password"
                          onChange={this.handleChange}
                          name="password"
                          value={this.state.fields.password}
                          class="form-control form-control-prepended"
                          placeholder="Enter your password"
                        />
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <span class="fa fa-key"></span>
                          </div>
                        </div>
                        {!!this.state.errors.password && (
                          <div class="alert alert-danger" role="alert">
                            {this.state.errors.password}
                          </div>
                        )}
                      </div>
                    </div>
                    <div class="form-group mb-1">
                      <button
                        onClick={(e) => {
                          e.preventDefault();
                          this.onsubmit();
                        }}
                        class="btn btn-block btn-primary"
                      >
                        Login
                      </button>
                    </div>
                    <div class="form-group text-center">
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          checked=""
                          id="remember"
                        />
                        <label class="custom-control-label" for="remember">
                          Remember me for 30 days
                        </label>
                      </div>
                    </div>
                    <div class="form-group text-center mb-0">
                      <a href="">Forgot password?</a> <br />
                      Don't have an account?{" "}
                      <a class="text-underline" href="/signup">
                        Sign up
                      </a>
                      <br />
                    </div>
                  </form>
                  <br />
                  <br />
                  {this.state.loading && (
                    <div class="loader loader-lg  mx-auto"></div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </React.Fragment>
    );
  }
}
