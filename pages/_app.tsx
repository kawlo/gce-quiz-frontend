import React, { useEffect } from "react";

import { ApolloProvider } from "@apollo/client";
import { AppProps } from "next/dist/next-server/lib/router/router";
import { useApollo } from "../lib/useApollo";
import Layout from "../components/Layout";
import Bootstrap from "../lib/bootstrap";
import { DefaultSeo } from "next-seo";

//csss
import "../public/assets/vendor/perfect-scrollbar.css";
import "../public/assets/css/app.css";
import "../public/assets/css/vendor-material-icons.css";
import "../public/assets/css/vendor-fontawesome-free.css";
import "../public/assets/css/vendor-ion-rangeslider.css";

//js

export default function App({ Component, pageProps }: AppProps) {
  const client = useApollo(pageProps);

  return (
    <ApolloProvider client={client}>
      <Bootstrap>
        <Layout>
          <DefaultSeo
            openGraph={{
              type: "website",
              locale: "en_GB",
              site_name: "GceQuiz",
            }}
            twitter={{
              handle: "@cgcerevision",
              site: "@cgcerevision",
              cardType: "summary_large_image",
            }}
          />
          <Component {...pageProps} />
        </Layout>
      </Bootstrap>
    </ApolloProvider>
  );
}
