import { gql } from "@apollo/client";

import {
  FRAGMENT_PAGINATION_INFO,
  FRAGMENT_FORUM_QUIZ_BASE,
  FRAGMENT_QUIZ_BASE,
  FRAGMENT_FORUM_ARTICLE,
  FRAGMENT_FORUM_QUIZ_BASE_SITEMAP,
} from "./fragments";

export const CATEGORY_QUERY_PAGINATION = gql`
  query categoryQuizPagination($page: Int!) {
    categoryPagination(page: $page, sort: _ID_DESC) {
      count
      items {
        ...FragmentQuizBase
      }
      pageInfo {
        ...FragmentPaginationInfo
      }
    }
  }

  ${FRAGMENT_PAGINATION_INFO}
  ${FRAGMENT_FORUM_QUIZ_BASE}
`;

export const CATEGORY_BY_ID_QUERY = gql`
  query categoryQuizById($id: MongoID!) {
    categoryById(_id: $id) {
      ...FragmentQuizBase
    }
  }

  ${FRAGMENT_FORUM_QUIZ_BASE}
`;
export const CATEGORY_BY_ID_QUERY_SITEMAP = gql`
  query categoryQuizById($id: MongoID!) {
    categoryById(_id: $id) {
      ...FragmentQuizBaseSiteMap
    }
  }

  ${FRAGMENT_FORUM_QUIZ_BASE_SITEMAP}
`;

export const CATEGORY_BY_SLUG_QUERY = gql`
  query categoryQuizBySlug($filter: FilterFindOneCategoryInput!) {
    categoryOne(filter: $filter) {
      ...FragmentQuizBase
    }
  }

  ${FRAGMENT_FORUM_QUIZ_BASE}
`;

export const QUIZ_BY_SLUG_QUERY = gql`
  query quizOneBySlug($filter: FilterFindOneQuizInput!) {
    quizOne(filter: $filter) {
      ...FragmentQuestionBase
    }
  }

  ${FRAGMENT_QUIZ_BASE}
`;

export const ARTICLE_QUERY_BY_ONE = gql`
  query articleOneBySlug($filter: FilterFindOneBlogArticleInput!) {
    articleOne(filter: $filter) {
      ...FragementArticle
    }
  }
  ${FRAGMENT_FORUM_ARTICLE}
`;

export const ARTICLE_QUERY_PAGINATION = gql`
  query articlePagination($page: Int!) {
    articlePagination(page: $page, sort: _ID_DESC, filter: { tags: null }) {
      count
      items {
        ...FragementArticle
      }
      pageInfo {
        ...FragmentPaginationInfo
      }
    }
  }
  ${FRAGMENT_FORUM_ARTICLE}
  ${FRAGMENT_PAGINATION_INFO}
`;

export const QUIZ_MANY_QUERY = gql`
  query quizManyByCategoryId($filter: FilterFindManyQuizInput!) {
    quizMany(filter: $filter) {
      _id
      title
      slug
      meta
      description
      questions {
        _id
      }
    }
  }
`;
export const QUIZ_MANY_QUERY_SITEMAP = gql`
  query quizManyByCategoryId($filter: FilterFindManyQuizInput!) {
    quizMany(filter: $filter) {
      _id
      title
      slug
      updatedAt
      createdAt
    }
  }
`;
