import { gql } from "@apollo/client";

export const FRAGMENT_FORUM_QUIZ_BASE = gql`
  fragment FragmentQuizBase on Category {
    _id
    name
    slug
    createdAt
    children {
      name
      slug
      content
      _id
      image {
        url
      }
      meta
      children(limit: 100) {
        name
        slug
        content
        _id
        image {
          url
        }
        meta
        childrenIds
      }
    }
    meta
    content
    createdBy {
      email
      phone
      username
      name
    }
    image {
      url
    }
    childrenIds
    parentIds
  }
`;

export const FRAGMENT_FORUM_QUIZ_BASE_SITEMAP = gql`
  fragment FragmentQuizBaseSiteMap on Category {
    _id
    name
    slug
    createdAt
    children {
      name
      slug
      _id
      meta
      createdAt
      updatedAt
      children(limit: 1000) {
        name
        slug
        _id
        childrenIds
        createdAt
        updatedAt
        children(limit: 1000) {
          name
          slug
          _id
          childrenIds
          createdAt
          updatedAt
        }
      }
    }

    childrenIds
    parentIds
  }
`;

export const FRAGMENT_QUIZ_BASE = gql`
  fragment FragmentQuestionBase on Quiz {
    createdAt
    _id
    title
    slug
    description
    meta
    questions {
      _id
      hint
      content
      image {
        url
      }
      note
      answers {
        text
        isCorrect
        _id
        image {
          url
        }
      }
    }
    category {
      name
      slug
    }
  }
`;

export const FRAGMENT_PAGINATION_INFO = gql`
  fragment FragmentPaginationInfo on PaginationInfo {
    currentPage
    perPage
    pageCount
    itemCount
    hasNextPage
    hasPreviousPage
  }
`;

export const FRAGMENT_FORUM_ARTICLE = gql`
  fragment FragementArticle on BlogArticle {
    createdAt
    _id
    title
    slug
    content
    createdBy {
      name
      avatar {
        url
      }
    }
    featuredImageId
    featuredImage {
      url
    }
    meta
    keywords
    categoryId
    category {
      name
      slug
    }
    createdAt
  }
`;
