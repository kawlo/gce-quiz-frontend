/******/ (function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/ var installedModules = {}; // The require function
  /******/
  /******/ /******/ function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/ if (installedModules[moduleId]) {
      /******/ return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/ var module = (installedModules[moduleId] = {
      /******/ i: moduleId,
      /******/ l: false,
      /******/ exports: {},
      /******/
    }); // Execute the module function
    /******/
    /******/ /******/ modules[moduleId].call(
      module.exports,
      module,
      module.exports,
      __webpack_require__
    ); // Flag the module as loaded
    /******/
    /******/ /******/ module.l = true; // Return the exports of the module
    /******/
    /******/ /******/ return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/ __webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/ __webpack_require__.d = function (exports, name, getter) {
    /******/ if (!__webpack_require__.o(exports, name)) {
      /******/ Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter,
      });
      /******/
    }
    /******/
  }; // define __esModule on exports
  /******/
  /******/ /******/ __webpack_require__.r = function (exports) {
    /******/ if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
      /******/ Object.defineProperty(exports, Symbol.toStringTag, {
        value: "Module",
      });
      /******/
    }
    /******/ Object.defineProperty(exports, "__esModule", { value: true });
    /******/
  }; // create a fake namespace object // mode & 1: value is a module id, require it // mode & 2: merge all properties of value into the ns // mode & 4: return value when already ns object // mode & 8|1: behave like require
  /******/
  /******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.t = function (
    value,
    mode
  ) {
    /******/ if (mode & 1) value = __webpack_require__(value);
    /******/ if (mode & 8) return value;
    /******/ if (
      mode & 4 &&
      typeof value === "object" &&
      value &&
      value.__esModule
    )
      return value;
    /******/ var ns = Object.create(null);
    /******/ __webpack_require__.r(ns);
    /******/ Object.defineProperty(ns, "default", {
      enumerable: true,
      value: value,
    });
    /******/ if (mode & 2 && typeof value != "string")
      for (var key in value)
        __webpack_require__.d(
          ns,
          key,
          function (key) {
            return value[key];
          }.bind(null, key)
        );
    /******/ return ns;
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/ __webpack_require__.n = function (module) {
    /******/ var getter =
      module && module.__esModule
        ? /******/ function getDefault() {
            return module["default"];
          }
        : /******/ function getModuleExports() {
            return module;
          };
    /******/ __webpack_require__.d(getter, "a", getter);
    /******/ return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/ __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/ __webpack_require__.p = "/"; // Load entry module and return exports
  /******/
  /******/
  /******/ /******/ return __webpack_require__((__webpack_require__.s = 31));
  /******/
})(
  /************************************************************************/
  /******/ {
    /***/ "./node_modules/@babel/runtime/helpers/classCallCheck.js":
      /*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        module.exports = _classCallCheck;

        /***/
      },

    /***/ "./node_modules/@babel/runtime/helpers/createClass.js":
      /*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        module.exports = _createClass;

        /***/
      },

    /***/ "./src/js/settings/dropdown-dismissable.js":
      /*!*************************************************!*\
  !*** ./src/js/settings/dropdown-dismissable.js ***!
  \*************************************************/
      /*! exports provided: default */
      /***/ function (module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.r(__webpack_exports__);
        /* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
          /*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js"
        );
        /* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/ __webpack_require__.n(
          _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__
        );
        /* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
          /*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js"
        );
        /* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/ __webpack_require__.n(
          _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__
        );

        // import $ from 'jquery'

        var DropdownDismissable = (function () {
          var NAME = "dropdownDismissable";
          var DATA_KEY = "bs.dropdown-dismissable";
          var JQUERY_NO_CONFLICT = $.fn[NAME];

          var DropdownDismissable = /*#__PURE__*/ (function () {
            function DropdownDismissable(element) {
              _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(
                this,
                DropdownDismissable
              );

              Object.defineProperty(element, NAME, {
                configurable: true,
                writable: false,
                value: this,
              });
              this._element = element;
              this._dismiss = element.querySelector(
                '[data-dismiss="dropdown"]'
              );
              this._toggle = element.querySelector('[data-toggle="dropdown"]');

              this._init();

              this._addEventListeners();
            }

            _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(
              DropdownDismissable,
              [
                {
                  key: "_init",
                  value: function _init() {
                    $(".dropdown-menu", this._element)
                      .removeClass("show")
                      .removeAttr("style");

                    if ($(this._element).hasClass("show")) {
                      $(this._element).removeClass("show");
                      $(this._toggle).dropdown("toggle");
                    }

                    this._element._closable = false;
                  },
                },
                {
                  key: "_addEventListeners",
                  value: function _addEventListeners() {
                    this._dismiss.addEventListener(
                      "click",
                      this.dismiss.bind(this)
                    );
                  },
                },
                {
                  key: "_removeEventListeners",
                  value: function _removeEventListeners() {
                    this._dismiss.removeEventListener(
                      "click",
                      this.dismiss.bind(this)
                    );
                  }, // Static jQuery Interface
                },
                {
                  key: "dismiss",
                  // Public
                  value: function dismiss(event) {
                    this._element._closable = true;

                    if (!event) {
                      $(this._toggle).dropdown("toggle");
                    }
                  },
                },
                {
                  key: "destroy",
                  value: function destroy() {
                    this._removeEventListeners();

                    $.removeData(this._element, DATA_KEY);
                    $(this._element).dropdown("dispose");
                    this._element = null;
                  },
                },
              ],
              [
                {
                  key: "_jQueryInterface",
                  value: function _jQueryInterface(config) {
                    return this.each(function () {
                      var data = $(this).data(DATA_KEY);

                      if (!data) {
                        data = new DropdownDismissable(this);
                        $(this).data(DATA_KEY, data);
                      }

                      if (typeof config === "string") {
                        if (typeof data[config] === "undefined") {
                          throw new Error(
                            'No method named "'.concat(config, '"')
                          );
                        }

                        data[config]();
                      }
                    });
                  },
                },
              ]
            );

            return DropdownDismissable;
          })();

          $(document).on(
            {
              "show.bs.dropdown": function showBsDropdown() {
                this._closable = false;
              },
              "hide.bs.dropdown": function hideBsDropdown() {
                return this._closable === undefined || this._closable !== false;
              },
            },
            "[data-dropdown-dismissable]"
          ); ////////////
          // jQuery //
          ////////////

          $.fn[NAME] = DropdownDismissable._jQueryInterface;
          $.fn[NAME].Constructor = DropdownDismissable;

          $.fn[NAME].noConflict = function () {
            $.fn[NAME] = JQUERY_NO_CONFLICT;
            return DropdownDismissable._jQueryInterface;
          }; ////////////////
          // Initialize //
          ////////////////

          $("[data-dropdown-dismissable]").dropdownDismissable();
          return DropdownDismissable;
        })($);

        /* harmony default export */ __webpack_exports__[
          "default"
        ] = DropdownDismissable;

        /***/
      },

    /***/ 31:
      /*!*******************************************************!*\
  !*** multi ./src/js/settings/dropdown-dismissable.js ***!
  \*******************************************************/
      /*! no static exports found */
      /***/ function (module, exports, __webpack_require__) {
        module.exports = __webpack_require__(
          /*! /Users/demi/Documents/GitHub/admin-lema/src/js/settings/dropdown-dismissable.js */ "./src/js/settings/dropdown-dismissable.js"
        );

        /***/
      },

    /******/
  }
);
