/******/ (function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/ var installedModules = {}; // The require function
  /******/
  /******/ /******/ function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/ if (installedModules[moduleId]) {
      /******/ return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/ var module = (installedModules[moduleId] = {
      /******/ i: moduleId,
      /******/ l: false,
      /******/ exports: {},
      /******/
    }); // Execute the module function
    /******/
    /******/ /******/ modules[moduleId].call(
      module.exports,
      module,
      module.exports,
      __webpack_require__
    ); // Flag the module as loaded
    /******/
    /******/ /******/ module.l = true; // Return the exports of the module
    /******/
    /******/ /******/ return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/ __webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/ __webpack_require__.d = function (exports, name, getter) {
    /******/ if (!__webpack_require__.o(exports, name)) {
      /******/ Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter,
      });
      /******/
    }
    /******/
  }; // define __esModule on exports
  /******/
  /******/ /******/ __webpack_require__.r = function (exports) {
    /******/ if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
      /******/ Object.defineProperty(exports, Symbol.toStringTag, {
        value: "Module",
      });
      /******/
    }
    /******/ Object.defineProperty(exports, "__esModule", { value: true });
    /******/
  }; // create a fake namespace object // mode & 1: value is a module id, require it // mode & 2: merge all properties of value into the ns // mode & 4: return value when already ns object // mode & 8|1: behave like require
  /******/
  /******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.t = function (
    value,
    mode
  ) {
    /******/ if (mode & 1) value = __webpack_require__(value);
    /******/ if (mode & 8) return value;
    /******/ if (
      mode & 4 &&
      typeof value === "object" &&
      value &&
      value.__esModule
    )
      return value;
    /******/ var ns = Object.create(null);
    /******/ __webpack_require__.r(ns);
    /******/ Object.defineProperty(ns, "default", {
      enumerable: true,
      value: value,
    });
    /******/ if (mode & 2 && typeof value != "string")
      for (var key in value)
        __webpack_require__.d(
          ns,
          key,
          function (key) {
            return value[key];
          }.bind(null, key)
        );
    /******/ return ns;
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/ __webpack_require__.n = function (module) {
    /******/ var getter =
      module && module.__esModule
        ? /******/ function getDefault() {
            return module["default"];
          }
        : /******/ function getModuleExports() {
            return module;
          };
    /******/ __webpack_require__.d(getter, "a", getter);
    /******/ return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/ __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/ __webpack_require__.p = "/"; // Load entry module and return exports
  /******/
  /******/
  /******/ /******/ return __webpack_require__((__webpack_require__.s = 8));
  /******/
})(
  /************************************************************************/
  /******/ {
    /***/ "./src/js/page.dashboard.js":
      /*!**********************************!*\
  !*** ./src/js/page.dashboard.js ***!
  \**********************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        (function () {
          "use strict";

          $('[data-toggle="tab"]').on("hide.bs.tab", function (e) {
            $(e.target).removeClass("active");
          });
          Charts.init();

          var EarningsTraffic = function EarningsTraffic(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "line";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                elements: {
                  line: {
                    fill: "start",
                    backgroundColor: settings.charts.colors.area,
                  },
                },
              },
              options
            );
            var data = {
              labels: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
              ],
              datasets: [
                {
                  label: "Traffic",
                  data: [10, 2, 5, 15, 10, 12, 15, 25, 22, 30, 25, 40],
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Transactions = function Transactions(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "line";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        maxTicksLimit: 5,
                        callback: function callback(a) {
                          return "$" + a + "k";
                        },
                      },
                    },
                  ],
                },
                tooltips: {
                  callbacks: {
                    label: function label(a, e) {
                      var t = e.datasets[a.datasetIndex].label || "",
                        o = a.yLabel,
                        r = "";
                      return (
                        1 < e.datasets.length &&
                          (r +=
                            '<span class="popover-body-label mr-auto">' +
                            t +
                            "</span>"),
                        (r +=
                          '<span class="popover-body-value">$' + o + "k</span>")
                      );
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
              ],
              datasets: [
                {
                  label: "Transactions",
                  data: [8, 5, 9, 6, 45, 4, 35, 14, 55, 19, 25, 20],
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Products = function Products(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "line";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            var data = arguments.length > 3 ? arguments[3] : undefined;
            options = Chart.helpers.merge(
              {
                elements: {
                  line: {
                    fill: "start",
                    backgroundColor: settings.charts.colors.area,
                    tension: 0,
                    borderWidth: 1,
                  },
                  point: {
                    pointStyle: "circle",
                    radius: 5,
                    hoverRadius: 5,
                    backgroundColor: settings.colors.white,
                    borderColor: settings.colors.primary[700],
                    borderWidth: 2,
                  },
                },
                scales: {
                  yAxes: [
                    {
                      display: false,
                    },
                  ],
                  xAxes: [
                    {
                      display: false,
                    },
                  ],
                },
              },
              options
            );
            data = data || {
              labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
              datasets: [
                {
                  label: "Earnings",
                  data: [400, 200, 450, 460, 220, 380, 800],
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Courses = function Courses(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "line";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                elements: {
                  line: {
                    borderColor: settings.colors.success[700],
                    backgroundColor: settings.hexToRGB(
                      settings.colors.success[100],
                      0.5
                    ),
                  },
                  point: {
                    borderColor: settings.colors.success[700],
                  },
                },
              },
              options
            );
            Products(id, type, options);
          };

          var LocationDoughnut = function LocationDoughnut(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "doughnut";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                tooltips: {
                  callbacks: {
                    title: function title(a, e) {
                      return e.labels[a[0].index];
                    },
                    label: function label(a, e) {
                      var t = "";
                      return (t +=
                        '<span class="popover-body-value">' +
                        e.datasets[0].data[a.index] +
                        "%</span>");
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: ["United States", "United Kingdom", "Germany", "India"],
              datasets: [
                {
                  data: [25, 25, 15, 35],
                  backgroundColor: [
                    settings.colors.success[400],
                    settings.colors.danger[400],
                    settings.colors.primary[500],
                    settings.colors.gray[300],
                  ],
                  hoverBorderColor:
                    "dark" == settings.charts.colorScheme
                      ? settings.colors.gray[800]
                      : settings.colors.white,
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Billing = function Billing(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "doughnut";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                tooltips: {
                  callbacks: {
                    title: function title(a, e) {
                      return e.labels[a[0].index];
                    },
                    label: function label(a, e) {
                      var t = "";
                      return (t +=
                        '<span class="popover-body-value">' +
                        e.datasets[0].data[a.index] +
                        "%</span>");
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: ["Current Value", null],
              datasets: [
                {
                  data: [75, 25],
                  backgroundColor: [
                    settings.colors.primary[500],
                    settings.colors.gray[50],
                  ],
                  hoverBorderColor:
                    "dark" == settings.charts.colorScheme
                      ? settings.colors.gray[800]
                      : settings.colors.white,
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Gender = function Gender(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "roundedBar";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                barRoundness: 1.2,
                scales: {
                  xAxes: [
                    {
                      maxBarThickness: 15,
                    },
                  ],
                },
              },
              options
            );
            var data = {
              labels: ["10-17", "18-30", "35-45", "46-60", "61-79", "80+"],
              datasets: [
                {
                  label: "Female",
                  data: [25, 20, 30, 22, 17, 10],
                },
                {
                  label: "Male",
                  data: [15, 10, 20, 12, 7, 2],
                  backgroundColor: settings.colors.primary[100],
                },
              ],
            };
            Charts.create(id, type, options, data);
          }; ///////////////////
          // Create Charts //
          ///////////////////

          EarningsTraffic("#earningsTrafficChart");
          Transactions("#transactionsChart");
          LocationDoughnut("#locationDoughnutChart");
          Billing("#billingChart");
          Products("#productsChart");
          Courses("#coursesChart");
          Gender("#genderChart");
        })();

        /***/
      },

    /***/ 8:
      /*!****************************************!*\
  !*** multi ./src/js/page.dashboard.js ***!
  \****************************************/
      /*! no static exports found */
      /***/ function (module, exports, __webpack_require__) {
        module.exports = __webpack_require__(
          /*! /Users/demi/Documents/GitHub/admin-lema/src/js/page.dashboard.js */ "./src/js/page.dashboard.js"
        );

        /***/
      },

    /******/
  }
);
