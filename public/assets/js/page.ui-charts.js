/******/ (function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/ var installedModules = {}; // The require function
  /******/
  /******/ /******/ function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/ if (installedModules[moduleId]) {
      /******/ return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/ var module = (installedModules[moduleId] = {
      /******/ i: moduleId,
      /******/ l: false,
      /******/ exports: {},
      /******/
    }); // Execute the module function
    /******/
    /******/ /******/ modules[moduleId].call(
      module.exports,
      module,
      module.exports,
      __webpack_require__
    ); // Flag the module as loaded
    /******/
    /******/ /******/ module.l = true; // Return the exports of the module
    /******/
    /******/ /******/ return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/ __webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/ __webpack_require__.d = function (exports, name, getter) {
    /******/ if (!__webpack_require__.o(exports, name)) {
      /******/ Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter,
      });
      /******/
    }
    /******/
  }; // define __esModule on exports
  /******/
  /******/ /******/ __webpack_require__.r = function (exports) {
    /******/ if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
      /******/ Object.defineProperty(exports, Symbol.toStringTag, {
        value: "Module",
      });
      /******/
    }
    /******/ Object.defineProperty(exports, "__esModule", { value: true });
    /******/
  }; // create a fake namespace object // mode & 1: value is a module id, require it // mode & 2: merge all properties of value into the ns // mode & 4: return value when already ns object // mode & 8|1: behave like require
  /******/
  /******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.t = function (
    value,
    mode
  ) {
    /******/ if (mode & 1) value = __webpack_require__(value);
    /******/ if (mode & 8) return value;
    /******/ if (
      mode & 4 &&
      typeof value === "object" &&
      value &&
      value.__esModule
    )
      return value;
    /******/ var ns = Object.create(null);
    /******/ __webpack_require__.r(ns);
    /******/ Object.defineProperty(ns, "default", {
      enumerable: true,
      value: value,
    });
    /******/ if (mode & 2 && typeof value != "string")
      for (var key in value)
        __webpack_require__.d(
          ns,
          key,
          function (key) {
            return value[key];
          }.bind(null, key)
        );
    /******/ return ns;
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/ __webpack_require__.n = function (module) {
    /******/ var getter =
      module && module.__esModule
        ? /******/ function getDefault() {
            return module["default"];
          }
        : /******/ function getModuleExports() {
            return module;
          };
    /******/ __webpack_require__.d(getter, "a", getter);
    /******/ return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/ __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/ __webpack_require__.p = "/"; // Load entry module and return exports
  /******/
  /******/
  /******/ /******/ return __webpack_require__((__webpack_require__.s = 11));
  /******/
})(
  /************************************************************************/
  /******/ {
    /***/ "./src/js/page.ui-charts.js":
      /*!**********************************!*\
  !*** ./src/js/page.ui-charts.js ***!
  \**********************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        (function () {
          "use strict";

          Charts.init();

          var Performance = function Performance(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "line";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        callback: function callback(a) {
                          if (!(a % 10)) return "$" + a + "k";
                        },
                      },
                    },
                  ],
                },
                tooltips: {
                  callbacks: {
                    label: function label(a, e) {
                      var t = e.datasets[a.datasetIndex].label || "",
                        o = a.yLabel,
                        r = "";
                      return (
                        1 < e.datasets.length &&
                          (r +=
                            '<span class="popover-body-label mr-auto">' +
                            t +
                            "</span>"),
                        (r +=
                          '<span class="popover-body-value">$' + o + "k</span>")
                      );
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
              ],
              datasets: [
                {
                  label: "Performance",
                  data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40],
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Orders = function Orders(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "roundedBar";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                barRoundness: 1.2,
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        callback: function callback(a) {
                          if (!(a % 10)) return "$" + a + "k";
                        },
                      },
                    },
                  ],
                },
                tooltips: {
                  callbacks: {
                    label: function label(a, e) {
                      var t = e.datasets[a.datasetIndex].label || "",
                        o = a.yLabel,
                        r = "";
                      return (
                        1 < e.datasets.length &&
                          (r +=
                            '<span class="popover-body-label mr-auto">' +
                            t +
                            "</span>"),
                        (r +=
                          '<span class="popover-body-value">$' + o + "k</span>")
                      );
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
              ],
              datasets: [
                {
                  label: "Sales",
                  data: [25, 20, 30, 22, 17, 10, 18, 26, 28, 26, 20, 32],
                  barPercentage: 0.5,
                  barThickness: 20,
                },
              ],
            };
            Charts.create(id, type, options, data);
          };

          var Devices = function Devices(id) {
            var type =
              arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : "doughnut";
            var options =
              arguments.length > 2 && arguments[2] !== undefined
                ? arguments[2]
                : {};
            options = Chart.helpers.merge(
              {
                tooltips: {
                  callbacks: {
                    title: function title(a, e) {
                      return e.labels[a[0].index];
                    },
                    label: function label(a, e) {
                      var t = "";
                      return (t +=
                        '<span class="popover-body-value">' +
                        e.datasets[0].data[a.index] +
                        "%</span>");
                    },
                  },
                },
              },
              options
            );
            var data = {
              labels: ["Desktop", "Tablet", "Mobile"],
              datasets: [
                {
                  data: [60, 25, 15],
                  backgroundColor: [
                    settings.colors.primary[700],
                    settings.colors.success[300],
                    settings.colors.success[100],
                  ],
                  hoverBorderColor:
                    "dark" == settings.charts.colorScheme
                      ? settings.colors.gray[800]
                      : settings.colors.white,
                },
              ],
            };
            Charts.create(id, type, options, data);
          }; ///////////////////
          // Create Charts //
          ///////////////////

          Performance("#performanceChart");
          Performance("#performanceAreaChart", "line", {
            elements: {
              line: {
                fill: "start",
                backgroundColor: settings.charts.colors.area,
              },
            },
          });
          Orders("#ordersChart");
          Orders("#ordersChartSwitch");
          Devices("#devicesChart");
          $('[data-toggle="chart"]:checked').each(function (index, el) {
            Charts.add($(el));
          });
        })();

        /***/
      },

    /***/ 11:
      /*!****************************************!*\
  !*** multi ./src/js/page.ui-charts.js ***!
  \****************************************/
      /*! no static exports found */
      /***/ function (module, exports, __webpack_require__) {
        module.exports = __webpack_require__(
          /*! /Users/demi/Documents/GitHub/admin-lema/src/js/page.ui-charts.js */ "./src/js/page.ui-charts.js"
        );

        /***/
      },

    /******/
  }
);
