export const CLIENT_URL =
  process.env.NODE_ENV === "production"
    ? "https://v1.api.gcequiz.com/graphql"
    : "https://v1.api.gcequiz.com/graphql";
