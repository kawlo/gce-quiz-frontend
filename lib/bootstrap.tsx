import React, { Component, Fragment } from "react";

class Bootstrap extends Component {
  componentDidMount = () => {
    if (typeof window !== "undefined") {
      import("jquery").then(($) => {
        // window.$ = window.jQuery = $;
        // // jQuery must be installed to the `window`:
        require("../public/assets/vendor/popper.min.js");
        require("../public/assets/vendor/bootstrap.min.js");
        require("../public/assets/vendor/perfect-scrollbar.min.js");
        require("../public/assets/vendor/dom-factory.js");
        require("../public/assets/vendor/material-design-kit.js");
        // require("../public/assets/js/app.js");
      });
    }
  };

  render() {
    return <Fragment>{this.props.children}</Fragment>;
  }
}

export default Bootstrap;
