import { useMemo } from "react";
import { ApolloClient, ApolloLink, InMemoryCache } from "@apollo/client";
import { onError } from "apollo-link-error";
import { createUploadLink } from "apollo-upload-client";
import SecureLS from "secure-ls";
import { setContext } from "apollo-link-context";
import merge from "deepmerge";
import isEqual from "lodash/isEqual";
import { concatPagination } from "@apollo/client/utilities";
export const APOLLO_STATE_PROP_NAME = "__APOLLO_STATE__";

let apolloClient;

let tokens: any = undefined;
if (typeof window !== "undefined") {
  const ls = new SecureLS({ encodingType: "aes" });
  tokens = typeof window !== "undefined" ? ls.get("__kw") : null;
}

const CLIENT_URL =
  process.env.NODE_ENV === "production"
    ? "https://v1.api.gcequiz.com/graphql"
    : "https://v1.api.gcequiz.com/graphql";

const uploadLink = createUploadLink({ uri: CLIENT_URL });
const errorLink = onError(({ graphQLErrors, networkError, operation }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Path: ${path}`)
    );
  }

  if (networkError) {
    console.log(
      `[Network error ${operation.operationName}]: ${networkError.message}`
    );
  }
});
const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  if (typeof window !== "undefined") {
    const ls = new SecureLS({ encodingType: "aes" });
    let token = ls.get("__kw");

    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token.accessToken}` : null,
      },
    };
  } else {
    return {
      headers: {
        ...headers,
      },
    };
  }
});
export const createApolloClient = () => {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: ApolloLink.from([errorLink, authLink, uploadLink]),
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            QueryForumPostPagination: concatPagination(),
          },
        },
      },
    }),
    credentials: "same-origin",
  });
};

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) =>
          sourceArray.every((s) => !isEqual(d, s))
        ),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function addApolloState(client, pageProps) {
  if (pageProps?.props) {
    pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract();
  }

  return pageProps;
}

export function useApollo(pageProps) {
  const state = pageProps[APOLLO_STATE_PROP_NAME];
  const store = useMemo(() => initializeApollo(state), [state]);
  return store;
}
